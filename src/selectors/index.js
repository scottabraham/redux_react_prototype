import { createSelector } from 'reselect'

const getRiskAsessments = (state) => state.riskasessments;

export const getCompletedRiskAsessments = createSelector(
  [ getRiskAsessments ],
  (asessments) => {
    console.log('getCompletedRiskAsessments executing');
    return asessments.filter(function(asessment){
      return asessment.complete === true;
    });
  }
)

export const getCompletedRiskAsessmentsCompletedToday = createSelector(
  [ getCompletedRiskAsessments ],
  (asessments) => {
    console.log('getCompletedRiskAsessmentsCompletedToday executing');
    return asessments.filter(function(asessment){
      return asessment.completeDate === '2017-11-08';
    });
  }
)

const getUsers = (state) => state.users;

export const getUserNames = createSelector(
  [ getUsers ],
  (users) => {
    return users.map(function(user, i){
      return user.username;
    });
  }
)
