import React, { Component } from 'react';
import tab from './components/tab.js';

class App extends Component {
  render() {
    return (
      React.createElement(
        "div",
        {className: "app-wrapper"},
        null,

        // Tab 1: Users
        React.createElement(
          tab,
          {id: 1, type: ["users"], description: "Users"},
          null
        ),

        // Tab2: Users and assets
        React.createElement(
          tab,
          {id: 2, type: ["users", "assets"], description: "Users and Assets"},
          null
        ),

        // Tab3: Risk Assessments
        React.createElement(
          tab,
          {id: 3, type: ['riskasessments'], description: "Risk Assesments Only"},
          null
        )

      )
    );
  }
}

export default App;
