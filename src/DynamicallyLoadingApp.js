import React, { Component } from 'react';
import ComponentContainer from './ComponentContainer';

class DynamicallyLoadingApp extends Component {
  render() {
    return (
      React.createElement(
        "div",
        null,
        React.createElement(
          "h2",
          null,
          "Dynamic Components Loading"
        ),
        this.props.widgets.map(function(component,id){
          let Component = ComponentContainer[component.type];
          return React.createElement(
            Component,
            {name: component.name},
            null
          );

        })

      )
    );
  }
}

export default DynamicallyLoadingApp;
