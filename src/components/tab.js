import React from 'react';
import UserList from '../containers/userlist.js';
import AssetList from '../containers/assetlist.js';
import UserNameList from '../containers/usernamelist.js';
import RiskAssesmentList from '../containers/riskassesmentlist.js';

function widgetLoader (props){
  return props.type.map(function(widgetname, i){
    switch(widgetname){
      case ("users"):{
        return React.createElement(
          UserList,
          {key: i, parentid: props.id},
          null
        );
      }
      case ("assets"):{
        return React.createElement(
          AssetList,
          {key: i, parentid: props.id},
          null
        );
      }
      case("usernames"):{
        return React.createElement(
          UserNameList,
          {key: i, parentid: props.id},
          null
        )
      }
      case("riskasessments"):{
        return React.createElement(
          RiskAssesmentList,
          {key: i, parentid: props.id},
          null
        )
      }
      default:
        break;
    }
  })
}

function tab(props){
  return (
    React.createElement(
      "div",
      {className: "tab"},
      null,
      React.createElement(
        "h2",
        null,
        props.description
      ),
      widgetLoader(props)
    )
  );
}

export default tab;
