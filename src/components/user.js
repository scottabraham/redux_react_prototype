import React from 'react';

function user (parentid, user, updateEmailAction){
  let emailTextboxId = "email-" + parentid + user.id;
  return(

    React.createElement(
      "div",
      {key: user.id},
      React.createElement(
        "p",
        null,

        React.createElement(
          "span",
          {className: "user-username"},
          user.username
        ),

        React.createElement(
          "input",
          {type: "textbox", id: emailTextboxId, defaultValue: user.email},
          null
        ),

        React.createElement(
          "button",
          {onClick: () => (updateEmailAction(user.id, document.getElementById(emailTextboxId).value)) },
          "Update Email"
        )
      )
    )
  );
}

export default user;
