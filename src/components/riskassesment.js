import React from 'react';

function riskasessment (parentid, asessment){

  if (asessment !== undefined){
  return(


    React.createElement(
      "div",
      {key: asessment.id},
      React.createElement(
        "p",
        null,

        React.createElement(
          "div",
          {className: "riskasessment-description"},
          asessment.description
        ),
        React.createElement(
          "div",
          {className: "riskasessment-confidence"},
          "Confidence: " + asessment.confidence
        ),

        React.createElement(
          "div",
          null,
          "Justification: " + asessment.justification
        )
      ),
      React.createElement(
        "hr",null,null
      )
    )
  );
}
}

export default riskasessment;
