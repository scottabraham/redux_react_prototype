import React from 'react';

function asset (parentid, asset, ownerName, updateAssetAction){

  let descriptionTextboxId = "description-" + parentid + asset.id;

  return(

    React.createElement(
      "div",
      {key: asset.id},
      React.createElement(
        "p",
        null,

        React.createElement(
          "span",
          {className: "asset-description"},
          asset.assetname
        ),

        React.createElement(
          "span",
          {className: "asset-ownername"},
          "Owner: " + ownerName
        ),

        React.createElement(
          "input",
          {type: "textbox", id: descriptionTextboxId, defaultValue: asset.description},
          null
        ),

        React.createElement(
          "button",
          {onClick: () => (updateAssetAction(asset.id, document.getElementById(descriptionTextboxId).value)) },
          "Update Desc."
        )
      ),
      React.createElement(
        "hr",null,null
      )
    )
  );
}

export default asset;
