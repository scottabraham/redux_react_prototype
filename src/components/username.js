import React from 'react';

function username (parentid, username){

  return(
    React.createElement(
      "div",
      null,
      React.createElement(
        "p",
        null,

        React.createElement(
          "span",
          {className: "user-username"},
          username
        )
      )
    )
  );
}

export default username;
