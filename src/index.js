import React from 'react';
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import './index.css';
import App from './App';
import * as reducers from './reducers/index.js';
import { createStore, combineReducers} from 'redux'

// initial state
const initialState = {    users: [  {id: "user-100", username: "DavidSmith", email: "dave@test.com" },
                                {id: "user-200", username: "SophieJones", email: "sjones@gmail.com" },
                                {id: "user-300", username: "TinaMcDonald", email: "tmc@rodgers.ca" }],

                          assets: [ {id: "asset-900", assetname: "Asset1", description: "Some asset", ownerId: "user-200"},
                                {id: "asset-901", assetname: "Asset2", description: "Another asset", ownerId: "user-300"}],

                          riskasessments: [ {id: "ra-1", description: "Assesment of pressure safety valve 13544/28", confidence: 3, justification: "blah blah blah", complete: false, completeDate: null},
                                            {id: "ra-2", description: "Assesment of pressure safety valve 43433/13", confidence: 1, justification: "Broken part #3750.", complete: true, completeDate: '2017-11-08'},
                                            {id: "ra-3", description: "Assesment of pressure safety valve 10933/55", confidence: 5, justification: "blah blah blah", complete: true, completeDate: '2017-01-03'}]
                    };

// create store
const store = createStore(
  combineReducers({users: reducers.users, assets: reducers.assets, riskasessments: reducers.riskasessments}), initialState, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

store.subscribe(() => {
     console.log("Store updated!", store.getState());
});

render (React.createElement(
    Provider,
    { store: store },
    React.createElement(App, null, null)
), window.document.getElementById('root'));
