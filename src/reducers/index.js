export const users = function (state =[], action){

  switch(action.type){
    case "USER_EMAILUPDATED":{
      return state.map((user, i) => {
        if (user.id===action.id) {
        // Copy the object before mutating
          return Object.assign({}, user, {
            email: action.email
            })
        } else { return user; }
      });
    }
    default:
      return state;
  }
}

export const assets = function (state = [], action){

  switch(action.type){
    case "ASSET_DESCRIPTIONUPDATED":{
      return state.map(function(asset, i){
        if(asset.id===action.id){
          return Object.assign({},asset,{
            description: action.description          } )
        }else{
          return Object.assign({},asset,{
            description: asset.description
          } )
        }
      })
    }

    default:{
      return state;
    }
  }

}

export const riskasessments = (state = {}, action) => {
  switch(action.type){
    default: return state
  }
}
