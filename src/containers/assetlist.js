import React from 'react';
import {connect} from "react-redux";
import asset from '../components/asset.js';

/* Redux Stuff */
// const mapStateToProps = (state) => {
// return {
//        assets: state.assets,
//        users: getUserNames(state)
//    };
//  };

const mapStateToProps = (state) => {
  return {
    users: state.users,
    assets: state.assets
  };
};

 const mapDispatchToProps = (dispatch) => {
     return {
         onAssetDescriptionUpdated: (id, description) => {
             dispatch({
                 type: "ASSET_DESCRIPTIONUPDATED",
                 id: id,
                 description: description
             });
         }
     };
 };

 const renderCountProp = "renderCount";

 const options = {renderCountProp: renderCountProp};

/* End Redux Stuff */
/* >>>>>>>>>>>>> Main Entry Point <<<<<<<<<<<<<<*/
class AssetList extends React.Component {

  constructor(props){
    super(props);
    console.log("assetlist: " + props)
  }

  // shouldComponentUpdate(nextProps) {
  //       const differentAssets = !isEqual(this.props.assets, nextProps.assets);
  //       const differentUsers = !isEqual(this.props.users, nextProps.users);
  //       return differentAssets || differentUsers;
  //   }

  render(){

    console.log ('assetlist rendering');
    var d = new Date();
    let action = this.props.onAssetDescriptionUpdated;

    return(
      React.createElement(
        "div",
        {className: "assetlist"},

        this.props.assets.map(function(theasset, id){
          let owner = this.props.users.find(function(user){
            return user.id === theasset.ownerId;
          })
          return asset(this.props.parentid, theasset, owner.username, action);
        }, this),

        React.createElement(
          "p",
          null,
          "AssetList Render Count: " + this.props.renderCount
        ),

        React.createElement(
          "p",
          null,
          "Last render timestamp: " + d.toLocaleTimeString()
        )
     )
   );
 }

}

export default connect(mapStateToProps, mapDispatchToProps, null, options )(AssetList);
