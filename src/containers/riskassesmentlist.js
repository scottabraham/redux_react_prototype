import React from 'react';
import {connect} from "react-redux";
import riskassesment from '../components/riskassesment.js';
import {getCompletedRiskAsessmentsCompletedToday} from '../selectors/index.js';

/* Redux Stuff */
const mapStateToProps = (state) => {
return {
       riskasessments: getCompletedRiskAsessmentsCompletedToday(state)
   };
 };

 const mapDispatchToProps = (dispatch) => {
     return {

         }

 };

 const renderCountProp = "renderCount";

 const options = {renderCountProp: renderCountProp};

/* End Redux Stuff */

/* >>>>>>>>>>>>> Main Entry Point <<<<<<<<<<<<<<*/
class RiskAssesmentList extends React.Component {

render(){

    var d = new Date();
    console.log ('risk assesments rendering');

    let parentid = this.props.parentid;
    return(
      React.createElement(
        "div",
        {className: "riskassesmentlist"},
        this.props.riskasessments.map(function(theassesment, id){
          return riskassesment(parentid, theassesment)
        }),
          React.createElement(
          "p",
          null,
          "Last render timestamp: " + d.toLocaleTimeString()
        )
     )
   );
 }
}

export default connect(mapStateToProps, mapDispatchToProps, null, options )(RiskAssesmentList);
