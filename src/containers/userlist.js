import React from 'react';
import {connect} from "react-redux";
import user from '../components/user.js';
import {isEqual} from 'lodash'

/* Redux Stuff */
const mapStateToProps = (state) => {
return {
       users: state.users
   };
 };

 const mapDispatchToProps = (dispatch) => {
     return {
         onUserEmailUpdated: (id, email) => {
             dispatch({
                 type: "USER_EMAILUPDATED",
                 id: id,
                 email: email
             });
         }
     };
 };

 const renderCountProp = "renderCount";

 const options = {renderCountProp: renderCountProp};

/* End Redux Stuff */

/* >>>>>>>>>>>>> Main Entry Point <<<<<<<<<<<<<<*/
class UserList extends React.Component {

  constructor(props){
    super(props);
    console.log("userlist: " + props)
  }

  // shouldComponentUpdate(nextProps) {
  //     const differentUsers = !isEqual(this.props.users, nextProps.users);
  //     return differentUsers;
  // }

render(){

    console.log ('userlist rendering');
    var d = new Date();

    let action = this.props.onUserEmailUpdated;
    let parentid = this.props.parentid;
    return(
      React.createElement(
        "div",
        {className: "userlist"},
        this.props.users.map(function(theuser, id){
          return user(parentid, theuser, action)
        }),
        React.createElement(
          "p",
          null,
          "UserList Render Count: " + this.props.renderCount
        ),
        React.createElement(
          "p",
          null,
          "Last render timestamp: " + d.toLocaleTimeString()
        )
     )
   );
 }
}

export default connect(mapStateToProps, mapDispatchToProps, null, options )(UserList);
