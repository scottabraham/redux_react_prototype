import React from 'react';
import {connect} from "react-redux";
import username from '../components/username.js';
import { getUserNames } from '../selectors/index.js';

/* Redux Stuff */
const mapStateToProps = (state) => {
return {
       usernames: getUserNames(state)
   };
 };

 const mapDispatchToProps = (dispatch) => {
     return {}; // do nothing
  };

/* End Redux Stuff */

/* >>>>>>>>>>>>> Main Entry Point <<<<<<<<<<<<<<*/
class UserNameList extends React.Component {

  // shouldComponentUpdate(nextProps, nextState){
  //    return !isEqual(nextProps, this.props);
  // }

render(){

    console.log ('usernamelist rendering');
    var d = new Date();

    let parentid = this.props.parentid;
    return(
      React.createElement(
        "div",
        {className: "usernamelist"},
        this.props.usernames.map(function(theusername, id){
          return username(parentid, theusername)
        }),
        React.createElement(
          "p",
          null,
          "Last render timestamp: " + d.toLocaleTimeString()
        )
     )
   );
 }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserNameList);
